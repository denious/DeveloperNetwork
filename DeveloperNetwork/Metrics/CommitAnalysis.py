import sys
import os
import git
import csv

# parse arguments
repoUrl = sys.argv[1]
destPath = sys.argv[2]

# get repository reference
repo = None
if not os.path.isdir(destPath):
    repo = git.Repo.clone_from(repoUrl, destPath, branch='master')
else:
    repo = git.Repo(destPath)

# traverse all commits
authors = []
timezones = []
commitInfoList = {}
for commit in repo.iter_commits():
    
    # get author name
    author = commit.author.name
    
    # save author
    if not author in authors:
        authors.append(author)
    
    # ignore undtermined timezones
    if commit.author_tz_offset == 0:
        continue
    
    # save timezone
    if not commit.author_tz_offset in timezones:
        timezones.append(commit.author_tz_offset)
    
    # retrieve commit info
    commitInfo = dict(commitInfoList.get(author, dict(
            commitCount=0,
            sponsoredCommitCount=0)
    ))
    
    # save commit time and incrase commit count
    time = commit.authored_datetime
    commitInfo["commitCount"] += 1
    
    # check if commit was between 9 and 5
    if time.hour >= 9 and time.hour <= 17:
        commitInfo["sponsoredCommitCount"] += 1
    
    # save commit info
    commitInfoList[author] = commitInfo
    
# save count of unique authors and timezones
authorCount = len(authors)
timezoneCount = len(timezones)
    
# calculate amount of sponsored devs
sponsoredAuthorCount = 0
for infoKey in commitInfoList.keys():
    info = commitInfoList[infoKey]
    commitCount = int(info["commitCount"])
    sponsoredCommitCount = int(info["sponsoredCommitCount"])
    diff = sponsoredCommitCount / commitCount
    if diff >= .95:
        sponsoredAuthorCount += 1
    
# append to project info CSV
with open('ProjectAnalysis.csv', 'a', newline='') as f:
    w = csv.writer(f, delimiter=',')
    w.writerow(["AuthorCount",authorCount])
    w.writerow(["SponsoredAuthorCount",sponsoredAuthorCount])
    w.writerow(["TimezoneCount",timezoneCount])