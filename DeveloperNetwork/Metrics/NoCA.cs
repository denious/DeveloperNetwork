﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using DeveloperNetwork.Models;

namespace DeveloperNetwork
{
    public partial class Metrics
    {
        /// <summary>
        ///     Number of commits per author
        /// </summary>
        /// <param name="releases"></param>
        /// <param name="commits"></param>
        public static void NoCA(List<Commit> commits)
        {
            // grab distinct authors and their commit count
            var authorCommits = commits
                .GroupBy(o => o.Author)
                .ToDictionary(o => o.Key, o => o.Count());
            
            // output to file
            using (var streamWriter = new StreamWriter("Metrics/NoCA.csv"))
            using (var csvWriter = new CsvWriter(streamWriter))
                csvWriter.WriteRecords(authorCommits);
        }
    }
}
