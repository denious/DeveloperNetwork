﻿using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DeveloperNetwork
{
    public partial class Metrics
    {
        public static async Task ProjectInfo(
            string cleanProjectName, 
            string projectUriShorthand, 
            string repositoryDownloadDir)
        {
            // build arguments
            var projectUri = CleanArg($"https://github.com/{projectUriShorthand}");
            var repoDir = CleanArg($"{repositoryDownloadDir}/{cleanProjectName}");
            var arguments = $"/C python CommitAnalysis.py {projectUri} {repoDir}";

            // define new process
            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = arguments,
                UseShellExecute = false,
                WorkingDirectory = Directory.GetCurrentDirectory() + "/Metrics"
            };

            var completionSource = new TaskCompletionSource<object>();
            var process = new Process
            {
                EnableRaisingEvents = true,
                StartInfo = startInfo
            };

            // execute Python script
            process.Start();
            process.Exited += (sender, args) => completionSource.TrySetResult(true);

            // wait for completion
            await completionSource.Task;
        }

        /// <summary>
        /// Code taken from https://stackoverflow.com/a/6040946
        /// </summary>
        private static string CleanArg(string value)
        {
            value = Regex.Replace(value, @"(\\*)" + "\"", @"$1$1\" + "\"");
            value = "\"" + Regex.Replace(value, @"(\\+)$", @"$1$1") + "\"";

            return value;
        }
    }
}
